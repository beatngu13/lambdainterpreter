package org.bitbucket.beatngu13.lambdainterpreter;

import java.util.HashMap;
import java.util.Stack;

public class Solver {
	
	/*
	 * Rename indices of bound variables. A bound variable vi creates an 
	 * entry, where key = v and value = i.
	 */
	private HashMap<String, Integer> variableIndices = new HashMap<>();
	/*
	 * Local scope of bound variables. When the term ((\x.y) z) is being 
	 * renamed, the local scope of the abstraction (\x.y) contains x, but not y.
	 */
	private Stack<String> localScope = new Stack<>();
	/*
	 * Processed term, which also represents the abstract syntax tree (AST).
	 */
	private Term ast;
	
	/*
	 * Set up, perform and tear down renaming.
	 */
	public Term rename(Term term) {
		ast = term.clone();
		
		renameRecursive(ast);
		variableIndices.clear();
		
		return ast;
	}
	
	/*
	 * Recursive depth-first search on AST in order the rename variables.
	 */
	private void renameRecursive(Term term) {
		// (\v.t)
		if (term instanceof Abstraction) {
			Abstraction abstraction = (Abstraction) term;
			Variable variable = abstraction.variable;
			
			// Lambda bound variable.
			localScope.push(variable.name);
			putVariable(variable);
			renameRecursive(abstraction.term);
			localScope.pop();
		// (t t)
		} else if (term instanceof Application) {
			Application application = (Application) term;
			
			renameRecursive(application.left);
			renameRecursive(application.right);
		// v
		} else {
			Variable variable = (Variable) term;
			String name = variable.name;
			
			// True => bound variable, false => free variable.
			if (localScope.contains(name) && variableIndices.containsKey(name)) {
				variable.name = name + variableIndices.get(name);
			}
		}
	}
	
	/*
	 * Put a bound variable into the indices map and rename it.
	 */
	private void putVariable(Variable variable) {
		String name = variable.name;
		int i = variableIndices.containsKey(name) ? variableIndices.get(name) + 1 : 1;
	
		variable.name = name + i;
		variableIndices.put(name, i);
	}
	
	/*
	 * Set up, perform and tear down substitution.
	 */
	public Term substitute(Term term) {
		ast = term.clone();
		
		renameRecursive(ast);
		substituteRecursive(ast);
		variableIndices.clear();
		
		return ast;
	}
	
	/*
	 * Recursive depth-first search on AST in order to substitute terms.
	 */
	private void substituteRecursive(Term term) {
		// (\v.t)
		if (term instanceof Abstraction) {
			substituteRecursive(((Abstraction) term).term);
		// (t t)
		} else if (term instanceof Application) {
			Application application = (Application) term;
			Term left = application.left;
			
			if (left instanceof Abstraction) {
				Abstraction abstraction = (Abstraction) left;
				// ((\v.t1) t2) => [t2 / v] t1.
				Term t2 = application.right;
				Variable v = abstraction.variable;
				Term t1 = abstraction.term;
				// Set t1 as new AST root node.
				ast = t1;
				
				replaceVariable(v, t2, t1, abstraction);
				substituteRecursive(ast);
			} else {
				substituteRecursive(application.left);
				substituteRecursive(application.right);
			}
		}
	}
	
	/*
	 * Recursively do [t2 / v] t1.
	 */
	private void replaceVariable(Variable v, Term t2, Term t1, Term parent) {
		// (\v.t)
		if (t1 instanceof Abstraction) {
			Abstraction abstraction = (Abstraction) t1;
			
			replaceVariable(v, t2, abstraction.variable, abstraction);
			replaceVariable(v, t2, abstraction.term, abstraction);
		// (t t)
		} else if (t1 instanceof Application) {
			Application application = (Application) t1;
			
			replaceVariable(v, t2, application.left, application);
			replaceVariable(v, t2, application.right, application);
		// v
		} else {
			Variable variable = (Variable) t1;
			
			// True => t1 equals v, which must be replaced with t2 in the parent node.
			if (variable.name.equals(v.name)) {
				if (parent instanceof Abstraction) {
					((Abstraction) parent).term = t2.clone();
				} else if (parent instanceof Application) {
					Application application = (Application) parent;
					
					if (application.left.equals(t1)) {
						application.left = t2.clone();
					} else {
						application.right = t2.clone();
					}
				}
			}
		}
	}
	
}
