package org.bitbucket.beatngu13.lambdainterpreter;

public class Abstraction extends Term {
	
	Variable variable;
	Term term;
	
	public Abstraction(Variable variable, Term term) {
		this.variable = variable;
		this.term = term;
	}
	
	@Override
	public String toString() {
		return "(\u03bb" + variable + "." + term + ")";
	}

	@Override
	public Term clone() {
		return new Abstraction((Variable) variable.clone(), term.clone());
	}

}
