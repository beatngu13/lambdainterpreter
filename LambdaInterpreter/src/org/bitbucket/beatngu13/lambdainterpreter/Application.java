package org.bitbucket.beatngu13.lambdainterpreter;

public class Application extends Term {
	
	Term left;
	Term right;
	
	public Application(Term left, Term right) {
		this.left = left;
		this.right = right;
	}
	
	@Override
	public String toString() {
		return "(" + left + " " + right + ")";
	}

	@Override
	public Term clone() {
		return new Application(left.clone(), right.clone());
	}

}
