package org.bitbucket.beatngu13.lambdainterpreter;

public abstract class Term implements Cloneable {
	
	@Override
	public abstract Term clone();
	
}
