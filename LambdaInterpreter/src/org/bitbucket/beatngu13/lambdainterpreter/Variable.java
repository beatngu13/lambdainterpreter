package org.bitbucket.beatngu13.lambdainterpreter;

public class Variable extends Term {

	String name;

	public Variable(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return name;
	}

	@Override
	public Term clone() {
		return new Variable(name);
	}

}
