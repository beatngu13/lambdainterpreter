package org.bitbucket.beatngu13.lambdainterpreter;

public class Main {

	public static void main(String[] args) {
		Solver solver = new Solver();
		
		// \x.x
		Term term1 = new Abstraction(new Variable("x"), new Variable("x"));
		System.out.println("Term 1:        " + term1);
		System.out.println("Renamed 1:     " + solver.rename(term1));
		System.out.println("Substituted 1: " + solver.substitute(term1) + "\n");

		// (\x.(x y))
		Term term2 = new Abstraction(new Variable("x"), 
				new Application(new Variable("x"), new Variable("y")));
		System.out.println("Term 2:        " + term2);
		System.out.println("Renamed 2:     " + solver.rename(term2));
		System.out.println("Substituted 2: " + solver.substitute(term2) + "\n");

		// ((\x.(x y)) (\x.(x y)))
		Term term3 = new Application(
				new Abstraction(new Variable("x"), 
				new Application(new Variable("x"), new Variable("y"))),
				new Abstraction(new Variable("x"), 
				new Application(new Variable("x"), new Variable("y"))));
		System.out.println("Term 3:        " + term3);
		System.out.println("Renamed 3:     " + solver.rename(term3));
		System.out.println("Substituted 3: " + solver.substitute(term3) + "\n");

		// (\x.(\x.x))
		Term term4 = new Abstraction(new Variable("x"), 
				new Abstraction(new Variable("x"), new Variable("x")));
		System.out.println("Term 4:        " + term4);
		System.out.println("Renamed 4:     " + solver.rename(term4));
		System.out.println("Substituted 4: " + solver.substitute(term4) + "\n");

		// ((\x.(\y.(\z.((x y) z)))) (\x.y))
		Term term5 = new Application(
				new Abstraction(new Variable("x"),
				new Abstraction(new Variable("y"),
				new Abstraction(new Variable("z"),
				new Application(
				new Application(new Variable("x"), new Variable("y")), new Variable("z"))))),
				new Abstraction(new Variable("x"), new Variable("y")));
		System.out.println("Term 5:        " + term5);
		System.out.println("Renamed 5:     " + solver.rename(term5));
		System.out.println("Substituted 5: " + solver.substitute(term5) + "\n");

		// ((\x.(\y.(x y))) z)
		Term term6 = new Application(
				new Abstraction(new Variable("x"),
				new Abstraction(new Variable("y"), 
				new Application(new Variable("x"), new Variable("y")))), new Variable("z"));
		System.out.println("Term 6:        " + term6);
		System.out.println("Renamed 6:     " + solver.rename(term6));
		System.out.println("Substituted 6: " + solver.substitute(term6) + "\n");
		
		// ((\x.(\y.(\z.((x y) z)))) ((\x.x) (\x.x)))
		Term term7 = new Application(
				new Abstraction(new Variable("x"),
				new Abstraction(new Variable("y"),
				new Abstraction(new Variable("z"),
				new Application(
				new Application(new Variable("x"), new Variable("y")), new Variable("z"))))),
				new Application(
				new Abstraction(new Variable("x"), new Variable("x")),
				new Abstraction(new Variable("x"), new Variable("x"))));
		System.out.println("Term 7:        " + term7);
		System.out.println("Renamed 7:     " + solver.rename(term7));
		System.out.println("Substituted 7: " + solver.substitute(term7) + "\n");
	}

}
